import unittest


class MyTest(unittest.TestCase):

    def setUp(self):
        self.linked_list = LinkedList()
        self.linked_list.put(1, '1')
        self.linked_list.put(1, '1')

    def test_put(self):
        self.assertEqual(self.linked_list.put(1, '2'), True)

    def test_get(self):
        self.assertEqual(self.linked_list.get(1), '1')

    def test_delete(self):
        self.assertEqual(self.linked_list.delete(1), True)

    def test_size(self):
        self.assertEqual(self.linked_list.size(), 2)

    def test_indexOf(self):
        self.assertEqual(self.linked_list.indexOf('1'), 1)


class Node:

    def __init__(self, value, previous=None, next=None):
        self.value = value
        self.previous = previous
        self.next = next


class LinkedList:

    def __init__(self):
        self.length = 0
        self.head = None

    def put(self, position, value):
        node = Node(value)
        current_node = self.head
        before_node_to_put = None
        after_node_to_put = None
        count = 1
        if position < 1 or position > self.length + 1:
            raise Exception('Invalid position.')
        if position == 1:
            if self.length == 0:
                self.head = node
                self.length += 1
                return
            after_node_to_put = self.head
            self.head = node
            after_node_to_put.previous = node
            node.next = after_node_to_put
            node.previous = None
            self.length += 1
            return True
        while count < position:
            before_node_to_put = current_node
            after_node_to_put = current_node.next
            current_node = current_node.next
            count += 1
        before_node_to_put.next = node
        node.previous = before_node_to_put
        node.next = after_node_to_put
        after_node_to_put.previous = node
        self.length += 1
        return True

    def get(self, position):
        current_node = self.head
        count = 1
        if self.length == 0 or position < 1 or position > self.length:
            raise Exception("Invalid position")
        while count < position:
            current_node = current_node.next
            count += 1
        return current_node.value

    def delete(self, position):
        current_node = self.head
        count = 1
        previous_node = None
        next_node = None
        node_to_delete = None
        if self.length == 0 or position < 1 or position > self.length:
            raise Exception("Invalid position")
        if position == 1:
            new_head = current_node.next
            new_head.previous = None
            self.head = new_head
            self.length -= 1
            return True
        while count <= position:
            node_to_delete = current_node
            previous_node = current_node.previous
            next_node = current_node.next
            current_node = current_node.next
            count += 1
        previous_node.next = next_node
        next_node.previous = previous_node
        del node_to_delete
        self.length -= 1
        return True

    def size(self):
        return self.length

    def indexOf(self, value):
        count = 1
        current_node = self.head
        while current_node:
            if current_node.value == value:
                return count
            current_node = current_node.next
            count += 1

    def __repr__(self):
        current = self.head
        result = ''
        while current:
            result += '{} '.format(current.value)
            current = current.next
        return result


if __name__ == '__main__':
    unittest.main()
