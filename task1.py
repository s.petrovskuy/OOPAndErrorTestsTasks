class SalaryGivingError(Exception):

    def __init__(self, message):
        self.message = message

    def __repr__(self):
        return self.message


class NotEmployeeException(Exception):

    def __init__(self, message):
        self.message = message

    def __repr__(self):
        return self.message


class WrongEmployeeRoleError(Exception):

    def __init__(self, second_name):
        self.second_name = second_name

    def __repr__(self):
        return "Employee {} has unexpected role!".format(self.second_name)


class WrongManagerDepartmen(Exception):

    def __init__(self, message):
        self.message = message

    def __repr__(self):
        return self.message


class Employee:

    def __init__(self, first_name, second_name, salary, experience, manager=None):
        self.first_name = first_name
        self.second_name = second_name
        self.salary = salary
        self.experience = experience
        self.manager = manager

    def __repr__(self):
        return "{} {}, manager:{}, experience:{}".format(self.first_name, self.second_name, self.manager.second_name,
                                                         self.experience)


class Designer(Employee):

    def __init__(self, first_name, second_name, salary, experience, effectiveness_coefficient):
        super().__init__(first_name, second_name, salary, experience)
        if effectiveness_coefficient > 1:
            self.effectiveness_coefficient = 1
        elif effectiveness_coefficient < 0:
            self.effectiveness_coefficient = 0
        else:
            self.effectiveness_coefficient = effectiveness_coefficient


class Developer(Employee):

    def __init__(self, first_name, second_name, salary, experience):
        super().__init__(first_name, second_name, salary, experience)


class Manager(Employee):

    def __init__(self, first_name, second_name, salary, experience, developers_team, designers_team):
        super().__init__(first_name, second_name, salary, experience)
        self.developers_team = developers_team
        self.designers_team = designers_team

    def add_to_developers_team(self, employees):
        if len(employees) == 0:
            raise NotEmployeeException("There is no employee in team")
        for employee in employees:
            if isinstance(employee, Manager):
                raise WrongEmployeeRoleError(employee.second_name)
        self.developers_team += employees

    def add_to_designers_team(self, employees):
        if len(employees) == 0:
            raise NotEmployeeException("There is no employee in team")
        for employee in employees:
            if isinstance(employee, Manager):
                raise WrongEmployeeRoleError(employee.second_name)
        self.developers_team += employees


class Departments:

    def __init__(self, managers):
        self.managers = managers

    def pay_salary(self):
        for manager in self.managers:
            if len(manager.designers_team) == 0 or len(manager.developers_team) == 0:
                raise SalaryGivingError("{} {} don't have any employee in team".format(manager.first_name,
                                                                                       manager.second_name))
            print("{} {}: got salary: {}".format(manager.first_name, manager.second_name,
                                                 self.calculate_salary(manager)))
            for developer in manager.developers_team:
                print("{} {}: got salary: {}".format(manager.first_name, manager.second_name,
                                                     self.calculate_salary(developer)))
            for designer in manager.designers_team:
                print("{} {}: got salary: {}".format(manager.first_name, manager.second_name,
                                                     self.calculate_salary(designer)))

    def calculate_salary(self, employee):
        if employee.experience > 5:
            final_salary = employee.salary * 1.2 + 500
        elif employee.experience > 2:
            final_salary = employee.salary + 200
        else:
            final_salary = employee.salary
        if isinstance(employee, Manager):
            if len(employee.developers_team) + len(employee.developers_team) > 5:
                final_salary += 200
            if len(employee.developers_team) + len(employee.developers_team) > 10:
                final_salary += 300
            if (len(employee.developers_team) + len(employee.developers_team)) / 2 >= 2:
                final_salary *= final_salary
        elif isinstance(employee, Designer):
            final_salary *= employee.effectiveness_coefficient
        return final_salary

    def add_team_members(self, manager, array):
        if manager not in self.managers:
            raise WrongManagerDepartmen("This manager is not in this Department!")
        try:
            self.managers[self.managers.indexOf(manager)].add_to_developers_team(array)
        except NotEmployeeException as e:
            print(e.message)
        except WrongEmployeeRoleError as e:
            print(e)
